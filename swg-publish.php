<?php
/**
 * Plugin Name:     SWG Publish
 * Plugin URI:      http://sterlingwoodsgroup.com
 * Description:     Publish post types and taxonomies
 * Author:          Andre Gagnon
 * Author URI:      http://sterlingwoodsgroup.com
 * Text Domain:     swg-publish
 * Domain Path:     /languages
 * Version:         0.2.7
 *
 * @package         Swg_Publish
 */

// add custom post types, taxonomies
require plugin_dir_path( __FILE__) . '/taxonomies/topic.php';
require plugin_dir_path( __FILE__) . '/taxonomies/beer-style.php';
require plugin_dir_path( __FILE__) . '/taxonomies/recipe-type.php';
require plugin_dir_path( __FILE__) . '/taxonomies/date.php';
require plugin_dir_path( __FILE__) . '/taxonomies/issue.php';
require plugin_dir_path( __FILE__) . '/taxonomies/writer.php';
require plugin_dir_path( __FILE__) . '/taxonomies/import-source.php';
require plugin_dir_path( __FILE__) . '/taxonomies/grain-style.php';
require plugin_dir_path( __FILE__) . '/taxonomies/substitution-hops.php';

require plugin_dir_path( __FILE__) . '/post-types/article.php';
require plugin_dir_path( __FILE__) . '/post-types/mr-wizard.php';
require plugin_dir_path( __FILE__) . '/post-types/project.php';
require plugin_dir_path( __FILE__) . '/post-types/recipe.php';
require plugin_dir_path( __FILE__) . '/post-types/faq.php';
require plugin_dir_path( __FILE__) . '/post-types/resource.php';
require plugin_dir_path( __FILE__) . '/post-types/chart-grain.php';
require plugin_dir_path( __FILE__) . '/post-types/chart-hops.php';
require plugin_dir_path( __FILE__) . '/post-types/chart-yeast.php';
require plugin_dir_path( __FILE__) . '/post-types/supplier.php';
require plugin_dir_path( __FILE__) . '/post-types/landing-page.php';
require plugin_dir_path( __FILE__) . '/post-types/bootcamp.php';
require plugin_dir_path( __FILE__) . '/post-types/nanocon.php';

// remove line breaks from content
//remove_filter( 'the_content', 'wpautop' );
//remove_filter( 'the_excerpt', 'wpautop' );

//add_filter( 'the_content', 'swg_publish_wpautop_nobr' );
//add_filter( 'the_excerpt', 'swg_publish_wpautop_nobr' );
function swg_publish_wpautop_nobr( $content ) {
    return wpautop( $content, false );
}


// simplify admin menu
add_action( 'admin_init', 'swg_publish_remove_menu_pages' );
function swg_publish_remove_menu_pages() {
  // remove posts
  //remove_menu_page( 'edit.php' );
  // remove comments
  //remove_menu_page( 'edit-comments.php' );
  // remove pages
  //  remove_menu_page( 'edit.php?post_type=page' );
  //remove_menu_page( 'edit.php?post_type=faq' );

}

//add_filter('acf/settings/save_json', 'swg_publish_acf_json_save_point');

function swg_publish_acf_json_save_point( $path ) {
    // update path
    $path = plugin_dir_path( __FILE__ ) . '/acf-json';
    // return
    return $path;
}

//add_filter('acf/settings/load_json', 'swg_publish_acf_json_load_point');

function swg_publish_acf_json_load_point( $paths ) {
    // remove original path (optional)
    unset($paths[0]);
    // append path
    $paths[] = plugin_dir_path( __FILE__ ) . '/acf-json';
    // return
    return $paths;
}

// add filters to articles edit page
function swg_publish_cp_filter() {
    global $typenow;

    // select the custom taxonomy
    $taxonomies = array('date', 'topic',  'writer', 'import-source', 'beer-style' );
    //$taxonomies = array('date'  );

    // select the type of custom post
    if( $typenow == 'project' || $typenow == 'article' || $typenow == 'mr-wizard' || $typenow == 'recipe' ){

        foreach ($taxonomies as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);
            //var_dump($_GET);
            //var_dump($tax_slug);
            //var_dump($terms);
            if(count($terms) > 0) {
                echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
                echo "<option value=''>Show All $tax_name</option>";
                foreach ($terms as $term) {
                  echo '<option value='. $term->slug, ( isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug) ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
                }
                echo "</select>";
            }
        }
    }

}
add_action( 'restrict_manage_posts', 'swg_publish_cp_filter' );
