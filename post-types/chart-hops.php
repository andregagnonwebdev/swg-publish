<?php

function chart_hops_init() {
	register_post_type( 'chart-hops', array(
		'labels'            => array(
			'name'                => __( 'HOPs', 'swg-publish' ),
			'singular_name'       => __( 'HOP', 'swg-publish' ),
			'all_items'           => __( 'All HOPs', 'swg-publish' ),
			'new_item'            => __( 'New HOP', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New HOP', 'swg-publish' ),
			'edit_item'           => __( 'Edit HOP', 'swg-publish' ),
			'view_item'           => __( 'View HOP', 'swg-publish' ),
			'search_items'        => __( 'Search HOPs', 'swg-publish' ),
			'not_found'           => __( 'No HOPs found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No HOPs found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent HOP', 'swg-publish' ),
			'menu_name'           => __( 'HOPs', 'swg-publish' ),
		),
		'public'            => false,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => false,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-media-spreadsheet',
		'show_in_rest'      => true,
		'rest_base'         => 'chart-hops',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'chart_hops_init' );

function chart_hops_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['chart-hops'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('HOP updated. <a target="_blank" href="%s">View HOP</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('HOP updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('HOP restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('HOP published. <a href="%s">View HOP</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('HOP saved.', 'swg-publish'),
		8 => sprintf( __('HOP submitted. <a target="_blank" href="%s">Preview HOP</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('HOP scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview HOP</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('HOP draft updated. <a target="_blank" href="%s">Preview HOP</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'chart_hops_updated_messages' );
