<?php

function project_init() {
	register_post_type( 'project', array(
		'labels'            => array(
			'name'                => __( 'Projects', 'swg-publish' ),
			'singular_name'       => __( 'Project', 'swg-publish' ),
			'all_items'           => __( 'All Projects', 'swg-publish' ),
			'new_item'            => __( 'New project', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New project', 'swg-publish' ),
			'edit_item'           => __( 'Edit project', 'swg-publish' ),
			'view_item'           => __( 'View project', 'swg-publish' ),
			'search_items'        => __( 'Search projects', 'swg-publish' ),
			'not_found'           => __( 'No projects found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No projects found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent project', 'swg-publish' ),
			'menu_name'           => __( 'Projects', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-hammer',
		'show_in_rest'      => true,
		'rest_base'         => 'project',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'project_init' );

function project_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['project'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Project updated. <a target="_blank" href="%s">View project</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Project updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Project restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Project published. <a href="%s">View project</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Project saved.', 'swg-publish'),
		8 => sprintf( __('Project submitted. <a target="_blank" href="%s">Preview project</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview project</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Project draft updated. <a target="_blank" href="%s">Preview project</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'project_updated_messages' );
