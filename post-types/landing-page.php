<?php

function landing_page_init() {
	register_post_type( 'landing-page', array(
		'labels'            => array(
			'name'                => __( 'Landing Pages', 'swg-publish' ),
			'singular_name'       => __( 'Landing Page', 'swg-publish' ),
			'all_items'           => __( 'All Landing Pages', 'swg-publish' ),
			'new_item'            => __( 'New landing page', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New landing page', 'swg-publish' ),
			'edit_item'           => __( 'Edit landing page', 'swg-publish' ),
			'view_item'           => __( 'View landing page', 'swg-publish' ),
			'search_items'        => __( 'Search landing pages', 'swg-publish' ),
			'not_found'           => __( 'No landing pages found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No landing pages found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent landing page', 'swg-publish' ),
			'menu_name'           => __( 'Landing Pages', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'has_archive'       => false,
		'rewrite'           => array(  'slug' => 'landing-page', 'with_front' => false ),
		'query_var'         => true,
		'menu_icon'         => 'dashicons-carrot',
		'show_in_rest'      => true,
		'rest_base'         => 'landing_page',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'landing_page_init' );

function landing_page_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['landing-page'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Landing Page updated. <a target="_blank" href="%s">View landing_page</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Landing Page updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Landing Page restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Landing Page published. <a href="%s">View landing page</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Landing Page saved.', 'swg-publish'),
		8 => sprintf( __('Landing Page submitted. <a target="_blank" href="%s">Preview landing page</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Landing Page scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview landing page</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Landing Page draft updated. <a target="_blank" href="%s">Preview landing page</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'landing_page_updated_messages' );
