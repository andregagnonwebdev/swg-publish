<?php

function chart_yeast_init() {
	register_post_type( 'chart-yeast', array(
		'labels'            => array(
			'name'                => __( 'YEASTs', 'swg-publish' ),
			'singular_name'       => __( 'YEAST', 'swg-publish' ),
			'all_items'           => __( 'All YEASTs', 'swg-publish' ),
			'new_item'            => __( 'New YEAST', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New YEAST', 'swg-publish' ),
			'edit_item'           => __( 'Edit YEAST', 'swg-publish' ),
			'view_item'           => __( 'View YEAST', 'swg-publish' ),
			'search_items'        => __( 'Search YEASTs', 'swg-publish' ),
			'not_found'           => __( 'No YEASTs found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No YEASTs found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent YEAST', 'swg-publish' ),
			'menu_name'           => __( 'YEASTs', 'swg-publish' ),
		),
		'public'            => false,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => false,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-media-spreadsheet',
		'show_in_rest'      => true,
		'rest_base'         => 'chart-yeast',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'chart_yeast_init' );

function chart_yeast_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['chart-yeast'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('YEAST updated. <a target="_blank" href="%s">View YEAST</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('YEAST updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('YEAST restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('YEAST published. <a href="%s">View YEAST</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('YEAST saved.', 'swg-publish'),
		8 => sprintf( __('YEAST submitted. <a target="_blank" href="%s">Preview YEAST</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('YEAST scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview YEAST</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('YEAST draft updated. <a target="_blank" href="%s">Preview YEAST</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'chart_yeast_updated_messages' );
