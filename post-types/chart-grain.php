<?php

function chart_grain_init() {
	register_post_type( 'chart-grain', array(
		'labels'            => array(
			'name'                => __( 'GRAINs', 'swg-publish' ),
			'singular_name'       => __( 'GRAIN', 'swg-publish' ),
			'all_items'           => __( 'All GRAINs', 'swg-publish' ),
			'new_item'            => __( 'New GRAIN', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New GRAIN', 'swg-publish' ),
			'edit_item'           => __( 'Edit GRAIN', 'swg-publish' ),
			'view_item'           => __( 'View GRAIN', 'swg-publish' ),
			'search_items'        => __( 'Search GRAINs', 'swg-publish' ),
			'not_found'           => __( 'No GRAINs found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No GRAINs found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent GRAIN', 'swg-publish' ),
			'menu_name'           => __( 'GRAINs', 'swg-publish' ),
		),
		'public'            => false,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => false,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-media-spreadsheet',
		'show_in_rest'      => true,
		'rest_base'         => 'chart-grains',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'chart_grain_init' );

function chart_grain_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['chart-grain'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('GRAIN updated. <a target="_blank" href="%s">View GRAIN</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('GRAIN updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('GRAIN restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('GRAIN published. <a href="%s">View GRAIN</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('GRAIN saved.', 'swg-publish'),
		8 => sprintf( __('GRAIN submitted. <a target="_blank" href="%s">Preview GRAIN</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('GRAIN scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview GRAIN</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('GRAIN draft updated. <a target="_blank" href="%s">Preview GRAIN</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'chart_grain_updated_messages' );
