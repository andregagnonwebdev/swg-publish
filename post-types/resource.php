<?php

function resource_init() {
	register_post_type( 'resource', array(
		'labels'            => array(
			'name'                => __( 'Resources', 'swg-publish' ),
			'singular_name'       => __( 'Resource', 'swg-publish' ),
			'all_items'           => __( 'All Resources', 'swg-publish' ),
			'new_item'            => __( 'New resource', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New resource', 'swg-publish' ),
			'edit_item'           => __( 'Edit resource', 'swg-publish' ),
			'view_item'           => __( 'View resource', 'swg-publish' ),
			'search_items'        => __( 'Search resources', 'swg-publish' ),
			'not_found'           => __( 'No resources found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No resources found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent resource', 'swg-publish' ),
			'menu_name'           => __( 'Resources', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 29,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
		'has_archive'       => true,
		'rewrite'           => array(  'slug' => 'resource', 'with_front' => false ),
		'query_var'         => true,
		'menu_icon'         => 'dashicons-clipboard',
		'show_in_rest'      => true,
		'rest_base'         => 'resource',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'resource_init' );

function resource_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['resource'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Resource updated. <a target="_blank" href="%s">View resource</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Resource updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Resource restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Resource published. <a href="%s">View resource</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Resource saved.', 'swg-publish'),
		8 => sprintf( __('Resource submitted. <a target="_blank" href="%s">Preview resource</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Resource scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview resource</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Resource draft updated. <a target="_blank" href="%s">Preview resource</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'resource_updated_messages' );
