<?php

function nanocon_init() {
	register_post_type( 'nanocon', array(
		'labels'            => array(
			'name'                => __( 'Nanocons', 'swg-publish' ),
			'singular_name'       => __( 'Nanocon', 'swg-publish' ),
			'all_items'           => __( 'All Nanocons', 'swg-publish' ),
			'new_item'            => __( 'New nanocon', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New nanocon', 'swg-publish' ),
			'edit_item'           => __( 'Edit nanocon', 'swg-publish' ),
			'view_item'           => __( 'View nanocon', 'swg-publish' ),
			'search_items'        => __( 'Search nanocons', 'swg-publish' ),
			'not_found'           => __( 'No nanocons found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No nanocons found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent nanocon', 'swg-publish' ),
			'menu_name'           => __( 'Nanocons', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail',  'page-attributes' ),
		'has_archive'       => false,
		'rewrite'           => array(  'slug' => 'nanocon', 'with_front' => false ),
//		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-tickets-alt',
		'show_in_rest'      => true,
		'rest_base'         => 'nanocon',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'nanocon_init' );

function nanocon_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['nanocon'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Nanocon updated. <a target="_blank" href="%s">View nanocon</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Nanocon updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Nanocon restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Nanocon published. <a href="%s">View nanocon</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Nanocon saved.', 'swg-publish'),
		8 => sprintf( __('Nanocon submitted. <a target="_blank" href="%s">Preview nanocon</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Nanocon scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview nanocon</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Nanocon draft updated. <a target="_blank" href="%s">Preview nanocon</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'nanocon_updated_messages' );
