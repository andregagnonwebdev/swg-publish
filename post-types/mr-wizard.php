<?php

function mr_wizard_init() {
	register_post_type( 'mr-wizard', array(
		'labels'            => array(
			'name'                => __( 'Mr. Wizard', 'swg-publish' ),
			'singular_name'       => __( 'Mr. Wizard', 'swg-publish' ),
			'all_items'           => __( 'All Mr. Wizard', 'swg-publish' ),
			'new_item'            => __( 'New Mr. Wizard', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New Mr. Wizard', 'swg-publish' ),
			'edit_item'           => __( 'Edit Mr. Wizard', 'swg-publish' ),
			'view_item'           => __( 'View Mr. Wizard', 'swg-publish' ),
			'search_items'        => __( 'Search Mr. Wizard', 'swg-publish' ),
			'not_found'           => __( 'No Mr. Wizard found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No Mr. Wizard found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent Mr. Wizard', 'swg-publish' ),
			'menu_name'           => __( 'Mr. Wizard', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'mr-wizard',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'mr_wizard_init' );

function mr_wizard_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['mr-wizard'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Mr. Wizard updated. <a target="_blank" href="%s">View Mr. Wizard</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Mr. Wizard updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Mr. Wizard restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Mr. Wizard published. <a href="%s">View Mr. Wizard</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Mr. Wizard saved.', 'swg-publish'),
		8 => sprintf( __('Mr. Wizard submitted. <a target="_blank" href="%s">Preview Mr. Wizard</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Mr. Wizard scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Mr. Wizard</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Mr. Wizard draft updated. <a target="_blank" href="%s">Preview Mr. Wizard</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'mr_wizard_updated_messages' );
