<?php

function recipe_init() {
	register_post_type( 'recipe', array(
		'labels'            => array(
			'name'                => __( 'Recipes', 'swg-publish' ),
			'singular_name'       => __( 'Recipe', 'swg-publish' ),
			'all_items'           => __( 'All Recipes', 'swg-publish' ),
			'new_item'            => __( 'New recipe', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New recipe', 'swg-publish' ),
			'edit_item'           => __( 'Edit recipe', 'swg-publish' ),
			'view_item'           => __( 'View recipe', 'swg-publish' ),
			'search_items'        => __( 'Search recipes', 'swg-publish' ),
			'not_found'           => __( 'No recipes found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No recipes found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent recipe', 'swg-publish' ),
			'menu_name'           => __( 'Recipes', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-media-document',
		'show_in_rest'      => true,
		'rest_base'         => 'recipe',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'recipe_init' );

function recipe_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['recipe'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Recipe updated. <a target="_blank" href="%s">View recipe</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Recipe updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Recipe restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Recipe published. <a href="%s">View recipe</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Recipe saved.', 'swg-publish'),
		8 => sprintf( __('Recipe submitted. <a target="_blank" href="%s">Preview recipe</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Recipe scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview recipe</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Recipe draft updated. <a target="_blank" href="%s">Preview recipe</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'recipe_updated_messages' );
