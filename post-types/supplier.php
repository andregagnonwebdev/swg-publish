<?php

function supplier_init() {
	register_post_type( 'supplier', array(
		'labels'            => array(
			'name'                => __( 'Suppliers', 'swg-publish' ),
			'singular_name'       => __( 'Supplier', 'swg-publish' ),
			'all_items'           => __( 'All Suppliers', 'swg-publish' ),
			'new_item'            => __( 'New supplier', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New supplier', 'swg-publish' ),
			'edit_item'           => __( 'Edit supplier', 'swg-publish' ),
			'view_item'           => __( 'View supplier', 'swg-publish' ),
			'search_items'        => __( 'Search suppliers', 'swg-publish' ),
			'not_found'           => __( 'No suppliers found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No suppliers found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent supplier', 'swg-publish' ),
			'menu_name'           => __( 'Suppliers', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 29,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', ),
		'has_archive'       => false,
		'rewrite'           => array(  'slug' => 'suppliers', 'with_front' => false ),
		'query_var'         => true,
		'menu_icon'         => 'dashicons-clipboard',
		'show_in_rest'      => true,
		'rest_base'         => 'supplier',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'supplier_init' );

function supplier_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['supplier'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Supplier updated. <a target="_blank" href="%s">View supplier</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Supplier updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Supplier restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Supplier published. <a href="%s">View supplier</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Supplier saved.', 'swg-publish'),
		8 => sprintf( __('Supplier submitted. <a target="_blank" href="%s">Preview supplier</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Supplier scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview supplier</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Supplier draft updated. <a target="_blank" href="%s">Preview supplier</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'supplier_updated_messages' );
