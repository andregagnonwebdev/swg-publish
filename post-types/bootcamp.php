<?php

function bootcamp_init() {
	register_post_type( 'bootcamp', array(
		'labels'            => array(
			'name'                => __( 'Bootcamps', 'swg-publish' ),
			'singular_name'       => __( 'Bootcamp', 'swg-publish' ),
			'all_items'           => __( 'All Bootcamps', 'swg-publish' ),
			'new_item'            => __( 'New bootcamp', 'swg-publish' ),
			'add_new'             => __( 'Add New', 'swg-publish' ),
			'add_new_item'        => __( 'Add New bootcamp', 'swg-publish' ),
			'edit_item'           => __( 'Edit bootcamp', 'swg-publish' ),
			'view_item'           => __( 'View bootcamp', 'swg-publish' ),
			'search_items'        => __( 'Search bootcamps', 'swg-publish' ),
			'not_found'           => __( 'No bootcamps found', 'swg-publish' ),
			'not_found_in_trash'  => __( 'No bootcamps found in trash', 'swg-publish' ),
			'parent_item_colon'   => __( 'Parent bootcamp', 'swg-publish' ),
			'menu_name'           => __( 'Bootcamps', 'swg-publish' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'menu_position'			=> 30,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail',  'page-attributes' ),
		'has_archive'       => false,
		'rewrite'           => array(  'slug' => 'byo-boot-camps', 'with_front' => false ),
//		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-tickets-alt',
		'show_in_rest'      => true,
		'rest_base'         => 'bootcamp',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'bootcamp_init' );

function bootcamp_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['bootcamp'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Bootcamp updated. <a target="_blank" href="%s">View bootcamp</a>', 'swg-publish'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'swg-publish'),
		3 => __('Custom field deleted.', 'swg-publish'),
		4 => __('Bootcamp updated.', 'swg-publish'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Bootcamp restored to revision from %s', 'swg-publish'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Bootcamp published. <a href="%s">View bootcamp</a>', 'swg-publish'), esc_url( $permalink ) ),
		7 => __('Bootcamp saved.', 'swg-publish'),
		8 => sprintf( __('Bootcamp submitted. <a target="_blank" href="%s">Preview bootcamp</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Bootcamp scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview bootcamp</a>', 'swg-publish'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Bootcamp draft updated. <a target="_blank" href="%s">Preview bootcamp</a>', 'swg-publish'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'bootcamp_updated_messages' );
