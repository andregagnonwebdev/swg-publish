<?php

function import_source_init() {
	register_taxonomy( 'import-source', array( 'article', 'mr-wizard', 'recipe', 'project', 'resource' ), array(
		'hierarchical'      => true,
		'public'            => false,
		'show_in_nav_menus' => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Import Sources', 'swg-publish' ),
			'singular_name'              => _x( 'Import Source', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Import Sources', 'swg-publish' ),
			'popular_items'              => __( 'Popular Import Sources', 'swg-publish' ),
			'all_items'                  => __( 'All Import Sources', 'swg-publish' ),
			'parent_item'                => __( 'Parent Import Source', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Import Source:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Import Source', 'swg-publish' ),
			'update_item'                => __( 'Update Import Source', 'swg-publish' ),
			'add_new_item'               => __( 'New Import Source', 'swg-publish' ),
			'new_item_name'              => __( 'New Import Source', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Import Sources with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Import Sources', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Import Sources', 'swg-publish' ),
			'not_found'                  => __( 'No Import Sources found.', 'swg-publish' ),
			'menu_name'                  => __( 'Import Sources', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'import-source',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'import_source_init' );
