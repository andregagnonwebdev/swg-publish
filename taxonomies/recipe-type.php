<?php

function recipe_type_init() {
	register_taxonomy( 'recipe-type', array(  'recipe',  ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Recipe Types', 'swg-publish' ),
			'singular_name'              => _x( 'Recipe Type', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Recipe Types', 'swg-publish' ),
			'popular_items'              => __( 'Popular Recipe Types', 'swg-publish' ),
			'all_items'                  => __( 'All Recipe Types', 'swg-publish' ),
			'parent_item'                => __( 'Parent Recipe Type', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Recipe Type:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Recipe Type', 'swg-publish' ),
			'update_item'                => __( 'Update Recipe Type', 'swg-publish' ),
			'add_new_item'               => __( 'New Recipe Type', 'swg-publish' ),
			'new_item_name'              => __( 'New Recipe Type', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Recipe Types with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Recipe Types', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Recipe Types', 'swg-publish' ),
			'not_found'                  => __( 'No Recipe Types found.', 'swg-publish' ),
			'menu_name'                  => __( 'Recipe Types', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'recipe-type',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'recipe_type_init' );
