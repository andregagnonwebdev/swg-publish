<?php

function writer_init() {
	register_taxonomy( 'writer', array( 'article', 'mr-wizard', 'recipe', 'project', 'resource' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
//		'rewrite'           => true,
		'rewrite'           => array( 'slug' => 'writer', 'with_front' => false),
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Writers', 'swg-publish' ),
			'singular_name'              => _x( 'Writer', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search writers', 'swg-publish' ),
			'popular_items'              => __( 'Popular writers', 'swg-publish' ),
			'all_items'                  => __( 'All writers', 'swg-publish' ),
			'parent_item'                => __( 'Parent writer', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent writer:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit writer', 'swg-publish' ),
			'update_item'                => __( 'Update writer', 'swg-publish' ),
			'add_new_item'               => __( 'New writer', 'swg-publish' ),
			'new_item_name'              => __( 'New writer', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate writers with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove writers', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used writers', 'swg-publish' ),
			'not_found'                  => __( 'No writers found.', 'swg-publish' ),
			'menu_name'                  => __( 'Writers', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'writer',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'writer_init' );
