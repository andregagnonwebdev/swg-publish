<?php

function beer_style_init() {
	register_taxonomy( 'beer-style', array( 'article', 'mr-wizard', 'recipe', 'project', 'resource', 'chart-hops', 'chart-yeast' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Beer Styles', 'swg-publish' ),
			'singular_name'              => _x( 'Beer Style', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Beer Styles', 'swg-publish' ),
			'popular_items'              => __( 'Popular Beer Styles', 'swg-publish' ),
			'all_items'                  => __( 'All Beer Styles', 'swg-publish' ),
			'parent_item'                => __( 'Parent Beer Style', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Beer Style:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Beer Style', 'swg-publish' ),
			'update_item'                => __( 'Update Beer Style', 'swg-publish' ),
			'add_new_item'               => __( 'New Beer Style', 'swg-publish' ),
			'new_item_name'              => __( 'New Beer Style', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Beer Styles with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Beer Styles', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Beer Styles', 'swg-publish' ),
			'not_found'                  => __( 'No Beer Styles found.', 'swg-publish' ),
			'menu_name'                  => __( 'Beer Styles', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'beer-style',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'beer_style_init' );
