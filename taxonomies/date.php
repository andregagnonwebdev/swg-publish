<?php

function date_init() {
	register_taxonomy( 'date', array( 'article', 'mr-wizard', 'recipe', 'project', 'resource' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
//		'rewrite'           => true,
		'rewrite'           => array( 'slug' => 'source-issue', 'with_front' => false),

		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Dates', 'swg-publish' ),
			'singular_name'              => _x( 'Date', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search dates', 'swg-publish' ),
			'popular_items'              => __( 'Popular dates', 'swg-publish' ),
			'all_items'                  => __( 'All dates', 'swg-publish' ),
			'parent_item'                => __( 'Parent date', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent date:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit date', 'swg-publish' ),
			'update_item'                => __( 'Update date', 'swg-publish' ),
			'add_new_item'               => __( 'New date', 'swg-publish' ),
			'new_item_name'              => __( 'New date', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate dates with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove dates', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used dates', 'swg-publish' ),
			'not_found'                  => __( 'No dates found.', 'swg-publish' ),
			'menu_name'                  => __( 'Dates', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'date',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'date_init' );
