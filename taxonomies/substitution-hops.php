<?php

function substitution_hops_init() {
	register_taxonomy( 'substitution-hops', array( 'chart-hops' ), array(
		'hierarchical'      => true,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Hops Substitutions', 'swg-publish' ),
			'singular_name'              => _x( 'Hops Substitution', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Hops Substitutions', 'swg-publish' ),
			'popular_items'              => __( 'Popular Hops Substitutions', 'swg-publish' ),
			'all_items'                  => __( 'All Hops Substitutions', 'swg-publish' ),
			'parent_item'                => __( 'Parent Hops Substitution', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Hops Substitution:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Hops Substitution', 'swg-publish' ),
			'update_item'                => __( 'Update Hops Substitution', 'swg-publish' ),
			'add_new_item'               => __( 'New Hops Substitution', 'swg-publish' ),
			'new_item_name'              => __( 'New Hops Substitution', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Hops Substitutions with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Hops Substitutions', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Hops Substitutions', 'swg-publish' ),
			'not_found'                  => __( 'No Hops Substitutions found.', 'swg-publish' ),
			'menu_name'                  => __( 'Hops Substitutions', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'substitution-hops',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'substitution_hops_init' );
