<?php

function grain_style_init() {
	register_taxonomy( 'grain-style', array( 'chart-grain' ), array(
		'hierarchical'      => true,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Grain Styles', 'swg-publish' ),
			'singular_name'              => _x( 'Grain Style', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search Grain Styles', 'swg-publish' ),
			'popular_items'              => __( 'Popular Grain Styles', 'swg-publish' ),
			'all_items'                  => __( 'All Grain Styles', 'swg-publish' ),
			'parent_item'                => __( 'Parent Grain Style', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent Grain Style:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit Grain Style', 'swg-publish' ),
			'update_item'                => __( 'Update Grain Style', 'swg-publish' ),
			'add_new_item'               => __( 'New Grain Style', 'swg-publish' ),
			'new_item_name'              => __( 'New Grain Style', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate Grain Styles with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove Grain Styles', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used Grain Styles', 'swg-publish' ),
			'not_found'                  => __( 'No Grain Styles found.', 'swg-publish' ),
			'menu_name'                  => __( 'Grain Styles', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'grain-style',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'grain_style_init' );
