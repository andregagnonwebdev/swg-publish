<?php

function issue_init() {
	register_taxonomy( 'issue', array( 'article' ), array(
		'hierarchical'      => true,
		'public'            => false,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
//		'rewrite'           => true,
		'rewrite'           => array( 'slug' => 'issue'),
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Issues', 'swg-publish' ),
			'singular_name'              => _x( 'Issue', 'taxonomy general name', 'swg-publish' ),
			'search_items'               => __( 'Search issues', 'swg-publish' ),
			'popular_items'              => __( 'Popular issues', 'swg-publish' ),
			'all_items'                  => __( 'All issues', 'swg-publish' ),
			'parent_item'                => __( 'Parent issue', 'swg-publish' ),
			'parent_item_colon'          => __( 'Parent issue:', 'swg-publish' ),
			'edit_item'                  => __( 'Edit issue', 'swg-publish' ),
			'update_item'                => __( 'Update issue', 'swg-publish' ),
			'add_new_item'               => __( 'New issue', 'swg-publish' ),
			'new_item_name'              => __( 'New issue', 'swg-publish' ),
			'separate_items_with_commas' => __( 'Separate issues with commas', 'swg-publish' ),
			'add_or_remove_items'        => __( 'Add or remove issues', 'swg-publish' ),
			'choose_from_most_used'      => __( 'Choose from the most used issues', 'swg-publish' ),
			'not_found'                  => __( 'No issues found.', 'swg-publish' ),
			'menu_name'                  => __( 'Issues', 'swg-publish' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'issue',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'issue_init' );
